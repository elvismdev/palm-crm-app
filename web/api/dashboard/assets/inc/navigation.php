<?php


  if ( $this_page == "Dashboard" ) { $act_dashboard = "active"; }
  elseif ( $this_page == "App Users" ) { $act_app_users = "active"; }
  else if ( $this_page == "Provider List" ) { $act_provider_list = "active"; }
  else if ( $this_page == "Add Provider" ) { $act_provider_list = "active"; }
  else if ( $this_page == "Edit Provider" ) { $act_provider_list = "active"; }

  else if ( $this_page == "Calendar" ) { $act_calendar = "active"; }
  else if ( $this_page == "Site Visit Form" ) { $act_site_visit_form = "active"; }
  else if ( $this_page == "Reports" ) { $act_reports = "active"; }

?>

				<div class="sidebar sidebar-fixed" id="sidebar">

					<ul class="nav nav-list">
						<li class="<?php echo $act_dashboard; ?>">
							<a href="<?php echo $sub_folder; ?>">
								<i class="icon-dashboard"></i>
								<span class="menu-text"> Dashboard <?php echo $this_page; ?></span>
							</a>
						</li>

						<li class="<?php echo $act_app_users; ?>">
							<a href="<?php echo $sub_folder; ?>App-Users/">
								<i class="icon-user"></i>
								<span class="menu-text"> App Users </span>
							</a>
						</li>

						<li class="<?php echo $act_provider_list; ?>">
							<a href="<?php echo $sub_folder; ?>Provider-List/">
								<i class="icon-book"></i>
								<span class="menu-text"> Provider List </span>
							</a>
						</li>

						<li class="<?php echo $act_calendar; ?>">
							<a href="<?php echo $sub_folder; ?>Calendar/">
								<i class="icon-calendar"></i>
								<span class="menu-text"> Calendar </span>
							</a>
						</li>

						<li class="<?php echo $act_site_visit_form; ?>">
							<a href="<?php echo $sub_folder; ?>Site-Visit-Form/">
								<i class="icon-list-alt"></i>
								<span class="menu-text"> Site Visit Form </span>
							</a>
						</li>

						<li class="<?php echo $act_reports; ?>">
							<a href="<?php echo $sub_folder; ?>Reports/">
								<i class="icon-edit"></i>
								<span class="menu-text"> Reports </span>
							</a>
						</li>

					</ul><!-- /.nav-list -->

<!--					<div class="sidebar-collapse" id="sidebar-collapse">
						<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
					</div>
-->
				</div>
