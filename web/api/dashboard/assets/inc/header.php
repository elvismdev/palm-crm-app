<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="author" content="Niner Labs, LLC" />
  	<meta name="description" content="Palm Medical Network - CRM Dashboard" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!-- basic styles -->

    <link rel="shortcut icon" href="/favicon.ico">  

		<link rel="stylesheet" href="<?php echo $sub_folder; ?>assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?php echo $sub_folder; ?>assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="<?php echo $sub_folder; ?>assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		<!-- page specific plugin styles -->
    <?php echo $page_specific_css; ?>
    
		<!-- fonts -->

		<link rel="stylesheet" href="<?php echo $sub_folder; ?>assets/css/ace-fonts.css" />

		<!-- ace styles -->

		<link rel="stylesheet" href="<?php echo $sub_folder; ?>assets/css/ace.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="<?php echo $sub_folder; ?>assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->

		<script src="<?php echo $sub_folder; ?>assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="<?php echo $sub_folder; ?>assets/js/html5shiv.js"></script>
		<script src="<?php echo $sub_folder; ?>assets/js/respond.min.js"></script>
		<![endif]-->
	</head>