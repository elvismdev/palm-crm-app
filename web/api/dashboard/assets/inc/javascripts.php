		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='<?php echo $sub_folder; ?>assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>
		<!-- <![endif]-->

		<!--[if IE]>
      <script type="text/javascript">
       window.jQuery || document.write("<script src='<?php echo $sub_folder; ?>assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
      </script>
    <![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='<?php echo $sub_folder; ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="<?php echo $sub_folder; ?>assets/js/bootstrap.min.js"></script>
		<script src="<?php echo $sub_folder; ?>assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->
    <?php echo $page_specific_js; ?>
	
		<!-- ace scripts -->

		<script src="<?php echo $sub_folder; ?>assets/js/ace-elements.min.js"></script>
		<script src="<?php echo $sub_folder; ?>assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->
