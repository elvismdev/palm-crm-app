<?php

  $company_name = "Palm CRM";
  $this_page = "Calendar";
  $page_title = $this_page." | ".$company_name;
  $sub_folder = "../";

  include($sub_folder.'assets/inc/global.php');

  $page_specific_css = "
    <link rel=\"stylesheet\" href=\"".$sub_folder."assets/css/fullcalendar.css\" />
		<link rel=\"stylesheet\" href=\"".$sub_folder."assets/css/chosen.css\" />
  ";
  $page_specific_js = "
		<script src=\"".$sub_folder."assets/js/jquery-ui-1.10.3.custom.min.js\"></script>
		<script src=\"".$sub_folder."assets/js/jquery.ui.touch-punch.min.js\"></script>
		<script src=\"".$sub_folder."assets/js/fullcalendar.min.js\"></script>
		<script src=\"".$sub_folder."assets/js/bootbox.min.js\"></script>
		<script src=\"".$sub_folder."assets/js/chosen.jquery.min.js\"></script>
  ";

	// DATABASE - GET PROVIDER LIST ==================================================================
	database_connect('palm-crm');
  
  $query_mysql = "SELECT unique_id, last_name, first_name, middle_name FROM pmn_doctors WHERE provider_status='1' ORDER BY last_name ASC, first_name ASC, middle_name ASC";
  $result_mysql = mysql_query($query_mysql);
  while ( $myrow = mysql_fetch_array($result_mysql)) {
    
    $unique_id = $myrow['unique_id'];
    $last_name = $myrow['last_name'];
    $first_name = $myrow['first_name'];
    $middle_name = $myrow['middle_name'];   
    
    $provider_list .= "<option value='".$unique_id."'>".$last_name.", ".$first_name." ".$middle_name."</option>";   
  
  }

  
	$user_query_mysql = "SELECT `user_id`, `full_name`, `color` FROM `pmn_user_list` WHERE color!='' AND user_id IN (SELECT user_id FROM pmn_calendar)";
	$user_result_mysql = mysql_query($user_query_mysql);
  while ( $user_myrow = mysql_fetch_array($user_result_mysql)) {
    $user_id = $user_myrow['user_id'];
    $user_name = $user_myrow['full_name'];
    $color = $user_myrow['color'];    
    $user_legend .= "
      <span class=\"external-event label-".$color."\" data-class=\"label-".$color."\">
        <i class=\"icon-user\"></i>
        ".$user_name."
      </span>
    ";
    
    $query_mysql = "SELECT event_id, doctor_id, app_datetime FROM pmn_calendar WHERE user_id='".$user_id."' AND status='1'";
    $result_mysql = mysql_query($query_mysql);
    while ( $myrow = mysql_fetch_array($result_mysql)) {
      
      $event_id = $myrow['event_id'];
      $doctor_id = $myrow['doctor_id'];
      $app_datetime = $myrow['app_datetime'];      
      
      $dr_query_mysql = "SELECT last_name, first_name, middle_name FROM pmn_doctors WHERE unique_id='".$doctor_id."'";
      $dr_result_mysql = mysql_query($dr_query_mysql);
      while ( $dr_myrow = mysql_fetch_array($dr_result_mysql)) {
        $last_name = $dr_myrow['last_name'];
        $first_name = $dr_myrow['first_name'];
        $middle_name = $dr_myrow['middle_name'];
      }
      
// { title: 'Test', start: '2014-06-09T16:00:00', end: '2014-06-09T16:00:00', allDay: false, className: 'label-important' }
      
      $event_list .= "{ title: '".$last_name.", ".$first_name." ".$middle_name."', start: '".$app_datetime."', end: '".$app_datetime."', allDay: false, className: 'label-".$color."', color: '".$event_id."' },";
      
      
    }
    
  }
 
  $event_list = substr_replace($event_list ,"",-1);
 
	database_close();		
	// ===============================================================================================

?>

  <!-- HEADER BEG -->
  <?php include($sub_folder.'assets/inc/header.php'); ?>
  <!-- HEADER END -->

	<body class="navbar-fixed breadcrumbs-fixed">

    <!-- TOPBAR BEG -->
    <?php include($sub_folder.'assets/inc/topbar.php'); ?>
    <!-- TOPBAR END -->

		<div class="main-container" id="main-container">

			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>

        <!-- NAVIGATION BEG -->
        <?php include($sub_folder.'assets/inc/navigation.php'); ?>
        <!-- NAVIGATION END -->

				<div class="main-content">
					<div class="breadcrumbs breadcrumbs-fixed" id="breadcrumbs">

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo $sub_folder; ?>">Home</a>
							</li>
							<li class="active"><?php echo $this_page; ?></li>
						</ul><!-- .breadcrumb -->

					</div>

					<div class="page-content">
						<div class="page-header">
							<h1><?php echo $this_page; ?>	</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
<!-- ========================================================================================================= -->
<!-- PAGE CONTENT BEGINS -->
<!-- ========================================================================================================= -->

								<div class="row">
									<div class="col-sm-12">
										<div class="space"></div>
										<div id="calendar"></div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-2">
                    <h4>User Legend</h4>
                    <?php echo $user_legend; ?>                            
									</div>
									<div class="col-sm-2">
                    <h4>&nbsp;</h4>
                    <button class="btn btn-info" data-toggle="modal" data-target="#addAppointment"><i class="icon-calendar align-top bigger-125 icon-on-right" style="padding-right:8px;"></i>Add Appointment</button> 
									</div>
                </div>

<!-- Modal -->
<div class="modal fade" id="addAppointment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add Appointment</h4>
      </div>
      <div class="modal-body">
      
        <div class="row">

          <div class="form-group">
            <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Select Provider </label>
            <div class="col-sm-8">

																<select class="chosen-select" data-placeholder="Select Provider">
                <option value="__">Select Provider</option>
                <?php echo $provider_list; ?>
              </select>

            </div>
          </div>

          <br>
        
          <div class="form-group">
            <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Select Date </label>
            <div class="col-sm-8">
              <input id="date" type="date" style="width:170px;" />
            </div>
          </div>

          <br>
          
          <div class="form-group">
            <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Select Time </label>
            <div class="col-sm-8">
              <select class="form-control" id="provider_state" style="width:170px;">
                <option value="__">Select Time</option>
                <option value="07:00">07:00am</option>
                <option value="07:15">07:15am</option>
                <option value="07:30">07:30am</option>
                <option value="07:45">07:45am</option>
                <option value="08:00">08:00am</option>
                <option value="08:15">08:15am</option>
                <option value="08:30">08:30am</option>
                <option value="08:45">08:45am</option>
                <option value="09:00">09:00am</option>
                <option value="09:15">09:15am</option>
                <option value="09:30">09:30am</option>
                <option value="09:45">09:45am</option>
                <option value="10:00">10:00am</option>
                <option value="10:15">10:15am</option>
                <option value="10:30">10:30am</option>
                <option value="10:45">10:45am</option>
                <option value="11:00">11:00am</option>
                <option value="11:15">11:15am</option>
                <option value="11:30">11:30am</option>
                <option value="11:45">11:45am</option>
                <option value="12:00">12:00am</option>
                <option value="12:15">12:15am</option>
                <option value="12:30">12:30am</option>
                <option value="12:45">12:45am</option>
                <option value="13:00">1:00pm</option>
                <option value="13:15">1:15pm</option>
                <option value="13:30">1:30pm</option>
                <option value="13:45">1:45pm</option>
                <option value="14:00">2:00pm</option>
                <option value="14:15">2:15pm</option>
                <option value="14:30">2:30pm</option>
                <option value="14:45">2:45pm</option>
                <option value="15:00">3:00pm</option>
                <option value="15:15">3:15pm</option>
                <option value="15:30">3:30pm</option>
                <option value="15:45">3:45pm</option>
                <option value="16:00">4:00pm</option>
                <option value="16:15">4:15pm</option>
                <option value="16:30">4:30pm</option>
                <option value="16:45">4:45pm</option>
                <option value="17:00">5:00pm</option>
                <option value="17:15">5:15pm</option>
                <option value="17:30">5:30pm</option>
                <option value="17:45">5:45pm</option>
                <option value="18:00">6:00pm</option>
                <option value="18:15">6:15pm</option>
                <option value="18:30">6:30pm</option>
                <option value="18:45">6:45pm</option>

              </select>
            </div>
          </div>
          
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary">Add Appointment</button>
      </div>
    </div>
  </div>
</div>



<!-- ========================================================================================================= -->
<!-- PAGE CONTENT ENDS -->
<!-- ========================================================================================================= -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->

			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

    <!-- JAVASCRIPTS BEG -->
    <?php include($sub_folder.'assets/inc/javascripts.php'); ?>
    <!-- JAVASCRIPTS END -->

		<script type="text/javascript">
			jQuery(function($) {

        /* initialize the calendar
        -----------------------------------------------------------------*/
      
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        
        var calendar = $('#calendar').fullCalendar({
           buttonText: {
            prev: '<i class="icon-chevron-left"></i>',
            next: '<i class="icon-chevron-right"></i>'
          },
        
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
          },
          events: [
          <?php echo $event_list; ?>
          ]
          ,
          editable: true,
          droppable: false, // this allows things to be dropped onto the calendar !!!
          drop: function(date, allDay) { // this function is called when something is dropped
          
            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');
            var $extraEventClass = $(this).attr('data-class');
            
            
            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);
            
            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            if($extraEventClass) copiedEventObject['className'] = [$extraEventClass];
            
            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
            
            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
              // if so, remove the element from the "Draggable Events" list
              $(this).remove();
            }
            
          }
          ,
          selectable: false,
          selectHelper: true,
          select: function(start, end, allDay) { },
          eventClick: function(calEvent, jsEvent, view) {
            
            var formatted_date_hour = calEvent.start.getHours();
            var formatted_date_minutes = calEvent.start.getMinutes();
            
            if ( formatted_date_minutes == 0 ) { formatted_date_minutes = "00"; }
            if ( formatted_date_hour > 12 ) { formatted_date_hour = formatted_date_hour - 12; ampm = "pm" } else { ampm = "am"; }
      
      
            var form = $("<form class='form-inline'><span style='width:200px; text-align:right; padding-right:8px;'>Provider Name:</span><span><b>" + calEvent.title + "</b></span><br><span style='width:200px; text-align:right; padding-right:8px;'>Appointment Time:</span><span><b>" + formatted_date_hour + ":" + formatted_date_minutes + ampm + "</b></span></form>");
            
            var div = bootbox.dialog({
              message: form,
            
              buttons: {
                "delete" : {
                  "label" : "<i class='icon-trash'></i> Delete Appointment",
                  "className" : "btn-sm btn-danger",
                  "callback": function() {
                    calendar.fullCalendar('removeEvents' , function(ev){
                      return (ev._id == calEvent._id);
                    })
                    delete_apointment(calEvent.color);
                  }
                } ,
                "close" : {
                  "label" : "<i class='icon-remove'></i> Close",
                  "className" : "btn-sm"
                } 
              }
      
            });
            
            form.on('submit', function(){
              calEvent.title = form.find("input[type=text]").val();
              calendar.fullCalendar('updateEvent', calEvent);
              div.modal("hide");
              return false;
            });
      
          }
          
        });
      
				//chosen plugin inside a modal will have a zero width because the select element is originally hidden
				//and its width cannot be determined.
				//so we set the width after modal is show
				$('#addAppointment').on('shown.bs.modal', function () {
					$(this).find('.chosen-container').each(function(){
						$(this).find('a:first-child').css('width' , '210px');
						$(this).find('.chosen-drop').css('width' , '210px');
						$(this).find('.chosen-search input').css('width' , '200px');
					});
				})
				//or you can activate the chosen plugin after modal is shown
				//this way select element becomes visible with dimensions and chosen works as expected

				$(".chosen-select").chosen(); 


				$('#addAppointment').on('shown', function () {
					$(this).find('.modal-chosen').chosen();
				})
      
      
      })

      function delete_apointment(app_details) {
        var url = "del_event.php?event_id=" + app_details;
        $.ajax({url:url,success:function(result){ }});        
      }

		</script>


	</body>
</html>
