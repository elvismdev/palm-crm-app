<?php

  $company_name = "Palm CRM";
  $this_page = "Add Provider";
  $page_title = "Add Provider | Provider List | ".$company_name;
  $sub_folder = "../../";

  include($sub_folder.'assets/inc/global.php');

  $page_specific_css = "
  ";
  $page_specific_js = "
		<script src=\"".$sub_folder."assets/js/jquery.dataTables.min.js\"></script>
		<script src=\"".$sub_folder."assets/js/jquery.dataTables.bootstrap.js\"></script>
  ";
  
  $back_up = str_replace("Edit/", "", strtok($_SERVER["REQUEST_URI"],'?'));

	// DATABASE - GET PROVIDER LIST ==================================================================
	database_connect('palm-crm');
  
  $query_mysql = "SELECT * FROM pmn_list_degree ORDER BY degree_desc ASC";
  $result_mysql = mysql_query($query_mysql);
  while ( $myrow = mysql_fetch_array($result_mysql)) {
    $degree_list .= "<option>".$myrow['degree_desc']."</option>";
  }

  $query_mysql = "SELECT * FROM pmn_list_specialty ORDER BY specialty_desc ASC";
  $result_mysql = mysql_query($query_mysql);
  while ( $myrow = mysql_fetch_array($result_mysql)) {
    $specialty_list .= "<option>".$myrow['specialty_desc']."</option>";
  }
  
  database_close();		
	// ===============================================================================================

?>

  <!-- HEADER BEG -->
  <?php include($sub_folder.'assets/inc/header.php'); ?>
  <!-- HEADER END -->

	<body class="navbar-fixed breadcrumbs-fixed">

    <!-- TOPBAR BEG -->
    <?php include($sub_folder.'assets/inc/topbar.php'); ?>
    <!-- TOPBAR END -->

		<div class="main-container" id="main-container">

			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>

        <!-- NAVIGATION BEG -->
        <?php include($sub_folder.'assets/inc/navigation.php'); ?>
        <!-- NAVIGATION END -->

				<div class="main-content">
					<div class="breadcrumbs breadcrumbs-fixed" id="breadcrumbs">

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo $sub_folder; ?>">Home</a>
							</li>
							<li><a href="<?php echo $sub_folder; ?>Provider-List/">Provider List</a></li>
							<li class="active"><?php echo $this_page; ?></li>
						</ul><!-- .breadcrumb -->

					</div>

					<div class="page-content">
						<div class="page-header">
							<h1><?php echo $this_page; ?>	</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
<!-- ========================================================================================================= -->
<!-- PAGE CONTENT BEGINS -->
<!-- ========================================================================================================= -->
                <div class="space-8"></div>

                <div class="space-8"></div>

                <form class="form-horizontal" role="form">

                  <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Last Name </label>
                    <div class="col-sm-9">
                      <input type="text" id="provider_last_name" placeholder="Last Name" class="col-xs-10 col-sm-5" maxlength="65" style="width:290px;" value="<?php echo $last_name; ?>" />
                    </div>
                  </div>
                  <div class="space-4"></div>

                  <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> First Name </label>
                    <div class="col-sm-9">
                      <input type="text" id="provider_first_name" placeholder="First Name" class="col-xs-10 col-sm-5" maxlength="65" style="width:290px;" value="<?php echo $first_name; ?>" />
                    </div>
                  </div>
                  <div class="space-4"></div>

                  <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Middle Name </label>
                    <div class="col-sm-9">
                      <input type="text" id="provider_middle_name" placeholder="Middle Name" class="col-xs-10 col-sm-5" maxlength="65" style="width:290px;" value="<?php echo $middle_name; ?>" />
                    </div>
                  </div>
                  
                  <hr>

                  <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Degree </label>
                    <div class="col-sm-9">
                      <select class="form-control" id="provider_degree" style="width:290px;">
                        <option value="">Degree</option>
                        <?php echo $degree_list; ?>
                      </select>
                    </div>
                  </div>
                  <div class="space-4"></div>

                  <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Specialty </label>
                    <div class="col-sm-9">
                      <select class="form-control" id="provider_specialty" style="width:290px;">
                        <option value="">Specialty</option>
                        <?php echo $specialty_list; ?>
                      </select>
                    </div>
                  </div>
                  
                  <hr>

                  <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Office Address 1 </label>
                    <div class="col-sm-9">
                      <input type="text" id="form-field-1" placeholder="Office Address 1" class="col-xs-10 col-sm-5" maxlength="65" style="width:290px;" value="<?php echo $office_address_1; ?>" />
                    </div>
                  </div>
                  <div class="space-4"></div>

                  <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Office Address 2 </label>
                    <div class="col-sm-9">
                      <input type="text" id="form-field-1" placeholder="Office Address 2" class="col-xs-10 col-sm-5" maxlength="65" style="width:290px;" value="<?php echo $office_address_2; ?>" />
                    </div>
                  </div>
                  <div class="space-4"></div>

                  <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Office City </label>
                    <div class="col-sm-9">
                      <input type="text" id="form-field-1" placeholder="Office City" class="col-xs-10 col-sm-5" maxlength="65" style="width:290px;" value="<?php echo $office_city; ?>" />
                    </div>
                  </div>
                  <div class="space-4"></div>

                  <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Office State </label>
                    <div class="col-sm-9">
                      <select class="form-control" id="provider_state" style="width:290px;">
                        <option value="__">Office State</option>
                        <option value="AL">Alabama</option>
                        <option value="AK">Alaska</option>
                        <option value="AZ">Arizona</option>
                        <option value="AR">Arkansas</option>
                        <option value="CA">California</option>
                        <option value="CO">Colorado</option>
                        <option value="CT">Connecticut</option>
                        <option value="DE">Delaware</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="HI">Hawaii</option>
                        <option value="ID">Idaho</option>
                        <option value="IL">Illinois</option>
                        <option value="IN">Indiana</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="ME">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NV">Nevada</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NM">New Mexico</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina</option>
                        <option value="ND">North Dakota</option>
                        <option value="OH">Ohio</option>
                        <option value="OK">Oklahoma</option>
                        <option value="OR">Oregon</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="SD">South Dakota</option>
                        <option value="TN">Tennessee</option>
                        <option value="TX">Texas</option>
                        <option value="UT">Utah</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="WA">Washington</option>
                        <option value="WV">West Virginia</option>
                        <option value="WI">Wisconsin</option>
                        <option value="WY">Wyoming</option>
                      </select>
                    </div>
                  </div>
                  <div class="space-4"></div>

                  <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Office Zip </label>
                    <div class="col-sm-9">
                      <input type="text" id="form-field-1" placeholder="Office Zip" class="col-xs-10 col-sm-5" maxlength="5" style="width:290px;" value="<?php echo $office_zip; ?>" />
                    </div>
                  </div>
                  
                  <hr>

                  <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Office Telephone </label>
                    <div class="col-sm-9">
                      <input type="text" id="form-field-1" placeholder="Office Telephone" class="col-xs-10 col-sm-5" style="width:290px;" value="<?php echo $office_tel; ?>" />
                    </div>
                  </div>
                  <div class="space-4"></div>

                  <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Office Fax </label>
                    <div class="col-sm-9">
                      <input type="text" id="form-field-1" placeholder="Office Fax" class="col-xs-10 col-sm-5"  style="width:290px;" value="<?php echo $office_fax; ?>" />
                    </div>
                  </div>
                  
                  <hr>

                  <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Office Manager </label>
                    <div class="col-sm-9">
                      <input type="text" id="form-field-1" placeholder="Office Manager" class="col-xs-10 col-sm-5" style="width:290px;" value="<?php echo $office_manager; ?>" />
                    </div>
                  </div>
                  <div class="space-4"></div>

                  <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Email Address </label>
                    <div class="col-sm-9">
                      <input type="text" id="form-field-1" placeholder="Email Address" class="col-xs-10 col-sm-5" style="width:290px;" value="<?php echo $email_address; ?>" />
                    </div>
                  </div>
                  
                  <hr>

                  <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Comments </label>
                    <div class="col-sm-9">
                      <textarea class="form-control" id="form-field-8" rows="4" placeholder="Default Text" style="width:290px; resize:none;"><?php echo $comments; ?></textarea>
                    </div>
                  </div>

                  <hr>

                  <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> </label>
                    <div class="col-sm-9">
                      <button class="btn btn-info"><i class="icon-save align-top bigger-125 icon-on-right" style="padding-right:8px;"></i>Add Provider</button> 
                    </div>
                  </div>
                  
                </form>


<!-- ========================================================================================================= -->
<!-- PAGE CONTENT ENDS -->
<!-- ========================================================================================================= -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->

			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

    <!-- JAVASCRIPTS BEG -->
    <?php include($sub_folder.'assets/inc/javascripts.php'); ?>
    <!-- JAVASCRIPTS END -->

    <script>
      <?php echo $select_degree; ?>    
      <?php echo $$select_specialty; ?>    
      <?php echo $select_state; ?>  
    </script>

	</body>
</html>
