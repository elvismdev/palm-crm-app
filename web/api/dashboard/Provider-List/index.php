<?php

  $company_name = "PALM CRM";
  $this_page = "Provider List";
  $page_title = $this_page." | ".$company_name;
  $sub_folder = "../";

  include($sub_folder.'assets/inc/global.php');

  $page_specific_css = "
  ";
  $page_specific_js = "
		<script src=\"".$sub_folder."assets/js/jquery.dataTables.min.js\"></script>
		<script src=\"".$sub_folder."assets/js/jquery.dataTables.bootstrap.js\"></script>
  ";

	// DATABASE - GET PROVIDER LIST ==================================================================
	database_connect('palm-crm');
  
  $query_mysql = "SELECT unique_id, last_name, first_name, middle_name, degree, specialty, office_address_1, office_address_2, office_city, office_state, office_zip, office_tel, office_fax, office_manager, last_update FROM pmn_doctors WHERE provider_status!='0' ORDER BY last_name ASC, first_name ASC";
  $result_mysql = mysql_query($query_mysql);
  while ( $myrow = mysql_fetch_array($result_mysql)) {

    $unique_id = $myrow['unique_id'];        
    $last_name = $myrow['last_name'];        
    $first_name = $myrow['first_name'];        
    $middle_name = $myrow['middle_name'];        
    $degree = $myrow['degree'];        
    $specialty = $myrow['specialty'];        
    $office_address_1 = $myrow['office_address_1'];        
    $office_address_2 = $myrow['office_address_2'];        
    $office_city = $myrow['office_city'];        
    $office_state = $myrow['office_state'];        
    $office_zip = $myrow['office_zip'];        
    $office_tel = $myrow['office_tel'];        
    $office_fax = $myrow['office_fax'];        
    $office_manager = $myrow['office_manager'];        
    $office_manager_xs = $myrow['office_manager'];        
    $last_update = date("m/d/y", strtotime($myrow['last_update']));        
    
    if ( $middle_name != '' ) { $middle_name = " ".$middle_name; }
    $full_name = $last_name.", ".$first_name.$middle_name."<br>Last: <i>".$last_update."</i></span>";
    
    if ( $office_address_2 != '' ) { $office_address_2 = ", ".$office_address_2; }
    $line_2 = "<br>".$office_city." ".$office_state." ".$office_zip;
    $full_address = $office_address_1.$office_address_2.$line_2;
    
    $provider_list .= "<tr><td class=\"hidden-480\" align=\"center\"><a href=\"Edit/?id=".$unique_id."\" class=\"tooltip-info\" data-rel=\"tooltip\" title=\"View\"><span class=\"blue\"><i class=\"icon-edit bigger-120\"></i></span></a></td><td>".$full_name."</td><td class=\"hidden-480\">".$degree."</td><td class=\"hidden-480\">".$specialty."</td><td class=\"hidden-480\">".$full_address."</td><td><a class=\"visible-xs\" href='tel:".$office_tel."'>".$office_tel."</a><span class=\"hidden-480\">".$office_tel."</span</td><td class=\"hidden-480\">".$office_fax."</td><td class=\"hidden-480\">".$office_manager."</td></tr>";
    
  }
 
	database_close();		
	// ===============================================================================================

?>

  <!-- HEADER BEG -->
  <?php include($sub_folder.'assets/inc/header.php'); ?>
  <!-- HEADER END -->

	<body class="navbar-fixed breadcrumbs-fixed">

    <!-- TOPBAR BEG -->
    <?php include($sub_folder.'assets/inc/topbar.php'); ?>
    <!-- TOPBAR END -->

		<div class="main-container" id="main-container">

			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>

        <!-- NAVIGATION BEG -->
        <?php include($sub_folder.'assets/inc/navigation.php'); ?>
        <!-- NAVIGATION END -->

				<div class="main-content">
					<div class="breadcrumbs breadcrumbs-fixed" id="breadcrumbs">

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo $sub_folder; ?>">Home</a>
							</li>
							<li class="active"><?php echo $this_page; ?></li>
						</ul><!-- .breadcrumb -->

					</div>

					<div class="page-content">
						<div class="page-header">
							<h1><?php echo $this_page; ?>	</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
<!-- ========================================================================================================= -->
<!-- PAGE CONTENT BEGINS -->
<!-- ========================================================================================================= -->

								<div class="row">
									<div class="col-xs-12">

										<div class="table-responsive">
											<table id="sample-table-2" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
                            <th class="hidden-480" style="width:10px;" align="center"></th>
														<th>Provider's Name</th>
														<th class="hidden-480" style="width:90px;">Degree</th>
														<th class="hidden-480" style="width:110px;">Specialty</th>
														<th class="hidden-480" style="width:250px;">Office Address</th>
														<th style="width:90px;">Office #</th>
														<th class="hidden-480" style="width:90px;">Fax #</th>
														<th class="hidden-480" style="width:150px;">Office Manager</th>
													</tr>
												</thead>
												<tbody>
                          <?php echo $provider_list; ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
                <div class="row">
                  <div class="col-xs-12">
                    <br>
                    <button class="btn btn-info" onClick="window.location.href='Add/'"><i class="icon-save align-top bigger-125 icon-on-right" style="padding-right:8px;"></i>Add Provider</button> 
                  </div>
                </div>

<!-- ========================================================================================================= -->
<!-- PAGE CONTENT ENDS -->
<!-- ========================================================================================================= -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->

			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

    <!-- JAVASCRIPTS BEG -->
    <?php include($sub_folder.'assets/inc/javascripts.php'); ?>
    <!-- JAVASCRIPTS END -->

		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false }, null, null, null, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }
				] } );
			})
		</script>


	</body>
</html>
