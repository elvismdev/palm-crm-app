<?php

  $company_name = "Palm CRM";
  $this_page = "Profile";
  $page_title = $this_page." | ".$company_name;
  $sub_folder = "../";

  include($sub_folder.'assets/inc/global.php');

  $page_specific_css = "
  ";
  $page_specific_js = "
		<script src=\"".$sub_folder."assets/js/x-editable/bootstrap-editable.min.js\"></script>
		<script src=\"".$sub_folder."assets/js/x-editable/ace-editable.min.js\"></script>
  ";

?>

  <!-- HEADER BEG -->
  <?php include($sub_folder.'assets/inc/header.php'); ?>
  <!-- HEADER END -->

	<body class="navbar-fixed breadcrumbs-fixed">

    <!-- TOPBAR BEG -->
    <?php include($sub_folder.'assets/inc/topbar.php'); ?>
    <!-- TOPBAR END -->

		<div class="main-container" id="main-container">

			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>

        <!-- NAVIGATION BEG -->
        <?php include($sub_folder.'assets/inc/navigation.php'); ?>
        <!-- NAVIGATION END -->

				<div class="main-content">
					<div class="breadcrumbs breadcrumbs-fixed" id="breadcrumbs">

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo $sub_folder; ?>">Home</a>
							</li>
							<li class="active"><?php echo $this_page; ?></li>
						</ul><!-- .breadcrumb -->

					</div>

					<div class="page-content">
						<div class="page-header">
							<h1><?php echo $this_page; ?>	</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
<!-- ========================================================================================================= -->
<!-- PAGE CONTENT BEGINS -->
<!-- ========================================================================================================= -->

								<div>
									<div id="user-profile-1" class="user-profile row">
										<div class="col-xs-12 col-sm-3 center">
											<div>
												<span class="profile-picture">
													<img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="<?php echo $sub_folder; ?>assets/avatars/<?php echo $current_user_profile; ?>" />
												</span>

												<div class="space-4"></div>

												<div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
													<div class="inline position-relative">
                            <i class="icon-circle light-green middle"></i>
                            &nbsp;
                            <span class="white"><?php echo $current_user_full; ?></span>
													</div>
												</div>
											</div>

  									</div>

										<div class="col-xs-12 col-sm-9">

											<div class="profile-user-info profile-user-info-striped">
												<div class="profile-info-row">
													<div class="profile-info-name"> Username </div>
													<div class="profile-info-value">
														<span class="editable" id="username">mcastillo</span>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Full Name </div>
													<div class="profile-info-value">
														<span class="editable" id="full_name">Martha Castillo</span>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Email </div>
													<div class="profile-info-value">
														<span class="editable" id="email">mcastillo@palmmednet.org</span>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Password </div>
													<div class="profile-info-value">
														<span class="editable" id="age"><button class="btn btn-xs btn-success" data-toggle="modal" data-target="#changePassword">Change Password</button></span>
													</div>
												</div>

											</div>

										</div>
									</div>
								</div>

<!-- Modal -->
<div class="modal fade" id="changePassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Change Password</h4>
      </div>
      <div class="modal-body">
      
<div class="row">
        <div class="form-group">
          <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Current Password </label>
          <div class="col-sm-8">
            <input type="password" id="provider_first_name" placeholder="Current Password" class="col-xs-10 col-sm-5" maxlength="65" style="width:290px;" />
          </div>
        </div>
        
        <br>
  
        <div class="form-group">
          <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> New Password </label>
          <div class="col-sm-8">
            <input type="password" id="provider_first_name" placeholder="New Password" class="col-xs-10 col-sm-5" maxlength="65" style="width:290px;" />
          </div>
        </div>

        <br>
  
        <div class="form-group">
          <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Verify New Password </label>
          <div class="col-sm-8">
            <input type="password" id="provider_first_name" placeholder="Verify New Password" class="col-xs-10 col-sm-5" maxlength="65" style="width:290px;" />
          </div>
        </div>
        <div class="space-4"></div>
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary">Change Password</button>
      </div>
    </div>
  </div>
</div>


<!-- ========================================================================================================= -->
<!-- PAGE CONTENT ENDS -->
<!-- ========================================================================================================= -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->

			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

    <!-- JAVASCRIPTS BEG -->
    <?php include($sub_folder.'assets/inc/javascripts.php'); ?>
    <!-- JAVASCRIPTS END -->

		<script type="text/javascript">
			jQuery(function($) {
			
				//editables on first profile page
				$.fn.editable.defaults.mode = 'inline';
				$.fn.editableform.loading = "<div class='editableform-loading'><i class='light-blue icon-2x icon-spinner icon-spin'></i></div>";
        $.fn.editableform.buttons = '<button type="submit" class="btn btn-info editable-submit"><i class="icon-ok icon-white"></i></button><button type="button" class="btn editable-cancel"><i class="icon-remove"></i></button>';				
				//editables 
			    $('#username').editable({
					type: 'text',
					name: 'username'
			    });
			    $('#full_name').editable({
					type: 'text',
					name: 'full_name'
			    });
			    $('#email').editable({
					type: 'text',
					name: 'email'
			    });

			});
		</script>

	</body>
</html>
