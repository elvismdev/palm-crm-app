<?php

namespace Palm\CRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ViewType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('screen')
            ->add('layout')
            ->add('left')
            ->add('width')
            ->add('height')
            ->add('top')
            ->add('visible')
            ->add('container')
            ->add('status', 'checkbox', array('required'  => false))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Palm\CRMBundle\Entity\View'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'palm_crmbundle_view';
    }
}
