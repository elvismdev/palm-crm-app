<?php

namespace Palm\CRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DoctorsType extends AbstractType
{
    private $em;

    public function __construct($em = null) {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $states = array(
            '__' => 'Office State',
            'AL' => "Alabama",
            'AK' => "Alaska",
            'AZ' => "Arizona",
            'AR' => "Arkansas",
            'CA' => "California",
            'CO' => "Colorado",
            'CT' => "Connecticut",
            'DE' => "Delaware",
            'DC' => "District Of Columbia",
            'FL' => "Florida",
            'GA' => "Georgia",
            'HI' => "Hawaii",
            'ID' => "Idaho",
            'IL' => "Illinois",
            'IN' => "Indiana",
            'IA' => "Iowa",
            'KS' => "Kansas",
            'KY' => "Kentucky",
            'LA' => "Louisiana",
            'ME' => "Maine",
            'MD' => "Maryland",
            'MA' => "Massachusetts",
            'MI' => "Michigan",
            'MN' => "Minnesota",
            'MS' => "Mississippi",
            'MO' => "Missouri",
            'MT' => "Montana",
            'NE' => "Nebraska",
            'NV' => "Nevada",
            'NH' => "New Hampshire",
            'NJ' => "New Jersey",
            'NM' => "New Mexico",
            'NY' => "New York",
            'NC' => "North Carolina",
            'ND' => "North Dakota",
            'OH' => "Ohio",
            'OK' => "Oklahoma",
            'OR' => "Oregon",
            'PA' => "Pennsylvania",
            'RI' => "Rhode Island",
            'SC' => "South Carolina",
            'SD' => "South Dakota",
            'TN' => "Tennessee",
            'TX' => "Texas",
            'UT' => "Utah",
            'VT' => "Vermont",
            'VA' => "Virginia",
            'WA' => "Washington",
            'WV' => "West Virginia",
            'WI' => "Wisconsin",
            'WY' => "Wyoming");

        $builder
            ->add('location', null, array('required' => false, 'label' => ' ', 'attr' => array('placeholder' => 'Location', 'class' => 'col-xs-10 col-sm-5', 'style' => 'width:290px;')))
            ->add('lastname', null, array('label' => ' ', 'attr' => array('placeholder' => 'Last Name', 'class' => 'col-xs-10 col-sm-5', 'style' => 'width:290px;')))
            ->add('firstname', null, array('label' => ' ', 'attr' => array('placeholder' => 'First Name', 'class' => 'col-xs-10 col-sm-5', 'style' => 'width:290px;')))
            ->add('middlename', null, array('required' => false, 'label' => ' ', 'attr' => array('placeholder' => 'Middle Name', 'class' => 'col-xs-10 col-sm-5', 'style' => 'width:290px;')))
            ->add('degree', 'choice', array('required' => false, 'label' => false, 'choices' => $this->getDegrees(), 'attr' => array('class' => 'form-control', 'style' => 'width:290px;')))
            ->add('specialty', 'choice', array('required' => false, 'label' => false, 'choices' => $this->getSpecialty(), 'attr' => array('class' => 'form-control', 'style' => 'width:290px;')))
            ->add('officeaddress1', null, array('label' => ' ', 'attr' => array('placeholder' => 'Office Address 1', 'class' => 'col-xs-10 col-sm-5', 'style' => 'width:290px;')))
            ->add('officeaddress2', null, array('required' => false, 'label' => ' ', 'attr' => array('placeholder' => 'Office Address 2', 'class' => 'col-xs-10 col-sm-5', 'style' => 'width:290px;')))
            ->add('officecity', null, array('label' => ' ', 'attr' => array('placeholder' => 'Office City', 'class' => 'col-xs-10 col-sm-5', 'style' => 'width:290px;')))
            ->add('officestate', 'choice', array('label' => false, 'choices' => $states, 'attr' => array('class' => 'form-control', 'style' => 'width:290px;')))
            ->add('officezip', null, array('label' => ' ', 'attr' => array('placeholder' => 'Office Zip', 'class' => 'col-xs-10 col-sm-5', 'style' => 'width:290px;')))
            ->add('officetel', null, array('label' => ' ', 'attr' => array('placeholder' => 'Office Telephone', 'class' => 'col-xs-10 col-sm-5', 'style' => 'width:290px;')))
            ->add('officefax', null, array('required' => false, 'label' => ' ', 'attr' => array('placeholder' => 'Office Fax', 'class' => 'col-xs-10 col-sm-5', 'style' => 'width:290px;')))
            ->add('officemanager', null, array('required' => false, 'label' => ' ', 'attr' => array('placeholder' => 'Office Manager', 'class' => 'col-xs-10 col-sm-5', 'style' => 'width:290px;')))
            ->add('comments', 'textarea', array('required' => false, 'label' => ' ', 'attr' => array('placeholder' => 'Comments', 'rows' => '4', 'class' => 'col-xs-10 col-sm-5', 'style' => 'width:290px; resize:none;')))
            ->add('emailaddress', null, array('required' => false, 'label' => ' ', 'attr' => array('placeholder' => 'Email', 'class' => 'col-xs-10 col-sm-5', 'style' => 'width:290px;')))
            ->add('lastupdate', null, array('required' => false, 'label' => ' ', 'attr' => array('readonly' => true, 'class' => 'col-xs-10 col-sm-5', 'style' => 'width:290px;')))
            ->add('providerstatus', 'checkbox', array('required' => false, 'label' => ' ', 'attr' => array('checked' => true, 'class' => 'col-xs-10 col-sm-5', 'style' => 'width:290px; resize:none;')));

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Palm\CRMBundle\Entity\Doctors'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'palm_crmbundle_doctors';
    }

    private function getDegrees()
    {
        $degrees = $this->em->getRepository('PalmCRMBundle:Degree')->findAll();

        $list = array();
        foreach ($degrees as $d) {
            $list[$d->getDegreevalue()] = $d->getDegreevalue();
        }
        return $list;
    }

    private function getSpecialty()
    {
        $specialty = $this->em->getRepository('PalmCRMBundle:Specialty')->findAll();

        $list = array();
        foreach ($specialty as $s) {
            $list[$s->getSpecialtyvalue()] = $s->getSpecialtyvalue();
        }
        return $list;
    }
}
