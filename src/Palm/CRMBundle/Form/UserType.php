<?php

namespace Palm\CRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $roles = array('App + Web Panel', 'App', 'Web');
        $webroles = array('Admin', 'Regular');

        $builder
            ->add('fullname')
            ->add('password', 'password', array('required' => false))
            ->add('photo', 'checkbox', array('required' => false))
            ->add('phone')
            ->add('status', 'checkbox', array('required' => false))
            ->add('type', 'choice', array('choices' => $roles))
            ->add('email')
            ->add('webrol', 'choice', array('choices' => $webroles))
            ->add('color')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Palm\CRMBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'palm_crmbundle_user';
    }
}
