<?php

namespace Palm\CRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ElementType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type')
            ->add('left')
            ->add('top')
            ->add('right')
            ->add('lblleft')
            ->add('lbltop')
            ->add('color')
            ->add('text')
            ->add('fontfamily')
            ->add('fontsize')
            ->add('fontweight')
            ->add('view', null, array('label' => 'Screen'))
            ->add('value')
            ->add('height')
            ->add('width')
            ->add('backgroundimage')
            ->add('backgroundfocusedimage')
            ->add('backgroundcolor')
            ->add('buttonid')
            ->add('options')
            ->add('id')
            ->add('hinttext')
            ->add('bordercolor')
            ->add('borderradius')
            ->add('paddingleft')
            ->add('status', 'checkbox', array('required'  => false))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Palm\CRMBundle\Entity\Element'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'palm_crmbundle_element';
    }
}
