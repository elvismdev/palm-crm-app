<?php

namespace Palm\CRMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;


class EmailController
{

    private  $em;
    private  $rootDir;

    function __construct($em, $rootDir)
    {
        $this->em = $em;
        $this->rootDir = $rootDir;
    }

    private function getAmazonSES()
    {
        require_once($this->rootDir . '/../vendor/amazonsesmailer/AmazonSESMailer.php');

        $amazonaws = $this->em->getRepository('PalmCRMBundle:Amazonaws')->findOneById(1);

        return new \AmazonSESMailer($amazonaws->getAwsId(), $amazonaws->getAwsSecretKey());
    }

    public function EmailNotificationAppointment($user, $provider, $calendar, $new = true, $mailer = 'AppointmentDetails')
    {
        $mailer = $this->em->getRepository('PalmCRMBundle:Mailer')->findOneBySource('AppointmentDetails');

        $amazonSESMailer = $this->getAmazonSES();
        $amazonSESMailer->ClearReplyTos();
        $amazonSESMailer->AddReplyTo($mailer->getFrom());
        $amazonSESMailer->SetFrom($mailer->getFrom(), $mailer->getFromname());

        // ADD THE TO EMAILS
        $amazonSESMailer->AddAddress($user->getEmail());

        // SUBJECT
        $amazonSESMailer->Subject = $mailer->getSubject();

        $title = ($new) ? 'New Appointment created.' : 'Appointment canceled.';

        $body = $mailer->getBody();
        $body = str_replace('[title]', $title, $body);
        $body = str_replace('[date]', $calendar->getAppdatetime(), $body);
        $body = str_replace('[name]', $provider->getName(), $body);
        $body = str_replace('[address]', $provider->getAddress(), $body);
        $body = str_replace('[phone]', $provider->getOfficetel(), $body);
        $body = str_replace('[fax]', $provider->getOfficefax(), $body);

        $amazonSESMailer->MsgHtml($body);

        if ($new) {
            // For calendar ics file and Add to Google Calendar link.
            $uid = $calendar->getId();
            $date = substr(str_replace(':', '', str_replace('-', '', \date('c', strtotime($calendar->getAppdatetime())))), 0, 15);
            $lastupdate = substr(str_replace(':', '', str_replace('-', '', \date('c', strtotime($calendar->getLastupdate())))), 0, 15);
            $fullname = $provider->getName();
            $email = $provider->getEmailaddress();
            $address = $provider->getAddress();
            $telf = $provider->getOfficetel();
            $fax = $provider->getOfficefax();

            $calDesc = "Provider%20Details%0D%0DName:%20$fullname%0DPhone:%20$telf%0DFax:%20$fax";

            //Link Add to google calendar
            $body .= '<br/><a href="https://www.google.com/calendar/render?action=TEMPLATE&text=Appointment&dates=' . $date . '/' . $date . '&details=' . $calDesc . '&location=' . $address . '&sf=true&output=xml">Add to Google Calendar</a>';

            $amazonSESMailer->MsgHtml($body);

            $ics = sprintf("BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//Niner Labs//Palm CRM//EN\r\nBEGIN:VEVENT\r\nUID:%s@palmcrm.com\r\nDTSTAMP:%s\r\nORGANIZER;CN=%s:MAILTO:johndoe@gmail.com\r\nDTSTART:%s\r\nDTEND:%s\r\nLOCATION:%s\r\nSUMMARY:Appointment\r\nDESCRIPTION:%s\r\nEND:VEVENT\r\nEND:VCALENDAR",
                $uid,
                $lastupdate,
                $fullname,
                $date,
                $date,
                str_replace(',', '\,', $address),
                str_replace('%20', ' ',
                    str_replace('%0D%0D', '\n\n',
                        str_replace('%0D', '\n', $calDesc)
                    )
                )
            );

            // Symfony way
            $fs = new Filesystem();

            if (!$fs->exists('/tmp/icalendar/'))
                $fs->mkdir('/tmp/icalendar/', 0700);

            $attachment = '/tmp/icalendar/invite_'.$lastupdate.'.ics';
            $fs->dumpFile($attachment, $ics);

            $attachment_name = 'invite.ics';
            $encoding = 'base64';
            $type = 'text/calendar';

            $amazonSESMailer->AddAttachment($attachment, $attachment_name, $encoding, $type);
        }

        $amazonSESMailer->Send();

        if ($new)
            $fs->remove($attachment);
    }
}