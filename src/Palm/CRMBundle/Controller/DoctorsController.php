<?php

namespace Palm\CRMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Palm\CRMBundle\Entity\Doctors;
use Palm\CRMBundle\Form\DoctorsType;
use Palm\CRMBundle\Entity\Doctorsarchive;

/**
 * Doctors controller.
 *
 */
class DoctorsController extends Controller
{

    /**
     * Lists all Doctors entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQuery('SELECT d FROM PalmCRMBundle:Doctors d ORDER BY d.lastname ASC, d.firstname ASC, d.middlename ASC')
            ->getResult();

        $status = '';
        if (isset($_SESSION['status'])) {
            $status = $_SESSION['status'];
            unset($_SESSION['status']);
        }

        return $this->render('PalmCRMBundle:Doctors:index.html.twig', array(
            'entities' => $entities,
            'status' => $status
        ));
    }

    /**
     * Lists all Doctors entities.
     *
     */
    public function indexAjaxAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = Request::createFromGlobals();

        // the URI being requested (e.g. /about) minus any query parameters
        $request->getPathInfo();

        $start = ($request->get('start')) ? (int)$request->get('start') : 0;
        $length = ($request->get('length')) ? (int)$request->get('length') : 10;
        $search = $request->get('search');
        $value = isset($search['value']) ? $search['value'] : '';
        $order = $request->get('order');
        $column = isset($order[0]['column']) ? $order[0]['column'] : 0;
        $order = isset($order[0]['dir']) ? $order[0]['dir'] : 'ASC';

        switch ($column) {
            case '1':
                $column = 'd.degree';
                break;
            case '2':
                $column = 'd.specialty';
                break;
            default:
                $column = 'd.lastname';
                break;
        }

        $count = $em->createQuery('SELECT Count(d) FROM PalmCRMBundle:Doctors d')->getSingleScalarResult();

        $query = 'SELECT CONCAT(\'<a href="\', d.id, \'/Edit\', \'" class="tooltip-info" data-rel="tooltip" title="View"><span class="blue"><i class="icon-edit bigger-120"></i></span></a>\') AS url,
            CONCAT(d.lastname, \', \', d.firstname, \' \', COALESCE(d.middlename, \' \')) AS name,
            d.degree, d.specialty,
            CONCAT(d.officeaddress1, \'<br>\', d.officecity, \' \', d.officestate, \' \', d.officezip) AS address,
            CONCAT(\'<a class="visible-xs" href="tel:\', d.officetel, \'">\', d.officetel, \'</a><span class="hidden-480">\', d.officetel, \'</span>\') AS officetel,
            d.officefax,
            d.officemanager,
            d.id
            FROM PalmCRMBundle:Doctors d ';
        if ($value) {
            $query .= 'WHERE d.lastname LIKE :value OR d.firstname LIKE :value OR d.middlename LIKE :value
                       OR d.degree LIKE :value OR d.specialty LIKE :value
                       OR d.officeaddress1 LIKE :value OR d.officecity LIKE :value OR d.officestate LIKE :value OR d.officezip LIKE :value
                       OR d.officetel LIKE :value OR d.officefax LIKE :value OR d.officemanager LIKE :value ';
        }
        $query .= "ORDER BY $column $order ";

        $entities = $em->createQuery($query);
        if ($value)
            $entities->setParameter('value', "%$value%");
        else {
            $entities = $entities
                ->setFirstResult($start)
                ->setMaxResults($length);
        }
        $entities = $entities->getResult();

        $ent = array();
        foreach ($entities as $e) {
            $query = 'SELECT c.appdatetime FROM PalmCRMBundle:Calendar c WHERE c.doctorid = :id ORDER BY c.appdatetime DESC';
            $result = $em->createQuery($query)
                ->setParameter('id', $e['id'])
                ->setMaxResults(1)
                ->getOneOrNullResult();

            $e['appdatetime'] = ($result) ? $result : null;

            if ($e['appdatetime']) {
                $e['appdatetime'] = date_format(new \DateTime($e['appdatetime']['appdatetime']), 'm/d/Y');
                $e['name'] .= '<br> Last: ' . $e['appdatetime'];
            }

            unset($e['appdatetime']);
            $ent[] = array_values($e);
        }

        $json = array(
            'recordsTotal' => (int)$count,
            'recordsFiltered' => (int)$count,
            'data' => $ent,
        );

        $response = new Response(json_encode($json));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Creates a new Doctors entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Doctors();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            // Timestamp
            $entity->setLastupdate(date('Y-m-d H:i:s'));

            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            $_SESSION['status'] = 'new';
            return $this->redirect($this->generateUrl('doctors'));
        }

        return $this->render('PalmCRMBundle:Doctors:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Doctors entity.
     *
     * @param Doctors $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Doctors $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new DoctorsType($em), $entity, array(
            'action' => $this->generateUrl('doctors_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Doctors entity.
     *
     */
    public function newAction()
    {
        $entity = new Doctors();
        $form = $this->createCreateForm($entity);

        return $this->render('PalmCRMBundle:Doctors:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Doctors entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PalmCRMBundle:Doctors')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Doctors entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PalmCRMBundle:Doctors:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),));
    }

    /**
     * Displays a form to edit an existing Doctors entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PalmCRMBundle:Doctors')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Doctors entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $intervals = $this->hoursRange(28800, 75600, 60 * 15);

        $status = '';
        if (isset($_SESSION['status'])) {
            $status = $_SESSION['status'];
            unset($_SESSION['status']);
        }

        return $this->render('PalmCRMBundle:Doctors:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'intervals' => $intervals,
            'id' => $id,
            'status' => $status,
        ));
    }

    /**
     * Creates a form to edit a Doctors entity.
     *
     * @param Doctors $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Doctors $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new DoctorsType($em), $entity, array(
            'action' => $this->generateUrl('doctors_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Doctors entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PalmCRMBundle:Doctors')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Doctors entity.');
        }

        $entity_archive = new Doctorsarchive(
            $entity->getComments(),
            $entity->getDegree(),
            date('Y-m-d H:i:s'),
            $entity->getEmailaddress(),
            $entity->getFirstname(),
            $entity->getLastname(),
            $entity->getLastupdate(),
            $entity->getLocation(),
            $entity->getMiddlename(),
            $entity->getOfficeaddress1(),
            $entity->getOfficeaddress2(),
            $entity->getOfficecity(),
            $entity->getOfficefax(),
            $entity->getOfficemanager(),
            $entity->getOfficestate(),
            $entity->getOfficetel(),
            $entity->getOfficezip(),
            $entity->getSpecialty(),
            $user->getId()
        );

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity_archive);

            // Timestamp
            $entity->setLastupdate(date('Y-m-d H:i:s'));

            $em->flush();

            $_SESSION['status'] = 'edit';
            return $this->redirect($this->generateUrl('doctors'));
        }

        return $this->render('PalmCRMBundle:Doctors:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Doctors entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PalmCRMBundle:Doctors')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Doctors entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('doctors'));
    }

    /**
     * Creates a form to delete a Doctors entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('doctors_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

    public function hoursRange($lower = 0, $upper = 86400, $step = 3600, $format = '')
    {
        $times = array();

        if (empty($format)) {
            $format = 'g:i a';
        }

        foreach (range($lower, $upper, $step) as $increment) {
            $increment = gmdate('H:i', $increment);

            list($hour, $minutes) = explode(':', $increment);

            $date = new \DateTime($hour . ':' . $minutes);

            $times[(string)$increment] = $date->format($format);
        }

        return $times;
    }
}
