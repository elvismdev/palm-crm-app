<?php

namespace Palm\CRMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Palm\CRMBundle\Entity\Mailer;
use Palm\CRMBundle\Form\MailerType;

/**
 * Mailer controller.
 *
 */
class MailerController extends Controller
{

    /**
     * Lists all Mailer entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PalmCRMBundle:Mailer')->findAll();

        $amazonaws = $em->getRepository('PalmCRMBundle:Amazonaws')->findAll();

        return $this->render('PalmCRMBundle:Mailer:index.html.twig', array(
            'entities' => $entities,
            'amazonaws' => $amazonaws
        ));
    }
    /**
     * Creates a new Mailer entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Mailer();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('mailer_show', array('id' => $entity->getId())));
        }

        return $this->render('PalmCRMBundle:Mailer:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Mailer entity.
    *
    * @param Mailer $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Mailer $entity)
    {
        $form = $this->createForm(new MailerType(), $entity, array(
            'action' => $this->generateUrl('mailer_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Mailer entity.
     *
     */
    public function newAction()
    {
        $entity = new Mailer();
        $form   = $this->createCreateForm($entity);

        return $this->render('PalmCRMBundle:Mailer:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Mailer entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PalmCRMBundle:Mailer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mailer entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PalmCRMBundle:Mailer:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Mailer entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PalmCRMBundle:Mailer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mailer entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PalmCRMBundle:Mailer:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Mailer entity.
    *
    * @param Mailer $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Mailer $entity)
    {
        $form = $this->createForm(new MailerType(), $entity, array(
            'action' => $this->generateUrl('mailer_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Mailer entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PalmCRMBundle:Mailer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mailer entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('mailer_edit', array('id' => $id)));
        }

        return $this->render('PalmCRMBundle:Mailer:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Mailer entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PalmCRMBundle:Mailer')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Mailer entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('mailer'));
    }

    /**
     * Creates a form to delete a Mailer entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('mailer_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function updateawsAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $aws_id = $this->getRequest()->get('aws_id');
        $aws_secret_key = $this->getRequest()->get('aws_secret_key');

        if (!$aws_id || !$aws_secret_key)
            return $this->redirect($this->generateUrl('mailer'));

        $entity = $em->getRepository('PalmCRMBundle:Amazonaws')->findOneById($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Amazonaws entity.');
        }

        $entity->setAwsId($aws_id);
        $entity->setAwsSecretKey($aws_secret_key);

        $em->flush();

        return $this->redirect($this->generateUrl('mailer'));
    }
}
