<?php

namespace Palm\CRMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Palm\CRMBundle\Entity\Container;
use Palm\CRMBundle\Form\ContainerType;

/**
 * Container controller.
 *
 */
class ContainerController extends Controller
{

    /**
     * Lists all Container entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PalmCRMBundle:Container')->findAll();

        return $this->render('PalmCRMBundle:Container:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Container entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Container();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('container_show', array('id' => $entity->getId())));
        }

        return $this->render('PalmCRMBundle:Container:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Container entity.
    *
    * @param Container $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Container $entity)
    {
        $form = $this->createForm(new ContainerType(), $entity, array(
            'action' => $this->generateUrl('container_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Container entity.
     *
     */
    public function newAction()
    {
        $entity = new Container();
        $form   = $this->createCreateForm($entity);

        return $this->render('PalmCRMBundle:Container:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Container entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PalmCRMBundle:Container')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Container entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PalmCRMBundle:Container:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Container entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PalmCRMBundle:Container')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Container entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PalmCRMBundle:Container:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Container entity.
    *
    * @param Container $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Container $entity)
    {
        $form = $this->createForm(new ContainerType(), $entity, array(
            'action' => $this->generateUrl('container_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Container entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PalmCRMBundle:Container')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Container entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('container_edit', array('id' => $id)));
        }

        return $this->render('PalmCRMBundle:Container:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Container entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PalmCRMBundle:Container')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Container entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('container'));
    }

    /**
     * Creates a form to delete a Container entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('container_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
