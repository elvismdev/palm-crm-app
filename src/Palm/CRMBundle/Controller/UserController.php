<?php

namespace Palm\CRMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Palm\CRMBundle\Entity\User;
use Palm\CRMBundle\Entity\Userarchive;
use Palm\CRMBundle\Form\UserType;

/**
 * User controller.
 *
 */
class UserController extends Controller
{

    /**
     * Lists all User entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PalmCRMBundle:User')->findAll();

        return $this->render('PalmCRMBundle:User:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new User entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new User();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setCode($entity->getPassword());
            $this->setSecurePassword($entity);

            // Modifiying User Role
            if ($entity->getType() == 1) $entity->setRoles('ROLE_USER'); else $entity->setRoles('ROLE_ADMIN');

            // Timestamp
            $entity->setLastupdate(date('Y-m-d H:i:s'));

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));
        }

        return $this->render('PalmCRMBundle:User:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new User entity.
     *
     */
    public function newAction()
    {
        $entity = new User();
        $form = $this->createCreateForm($entity);

        return $this->render('PalmCRMBundle:User:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a User entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PalmCRMBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PalmCRMBundle:User:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),));
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PalmCRMBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PalmCRMBundle:User:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing User entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PalmCRMBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $user_archive = new Userarchive(
            $entity->getCode(),
            $entity->getColor(),
            $entity->getEmail(),
            $entity->getFullname(),
            date('Y-m-d H:i:s'),
            $entity->getPassword(),
            $entity->getPhone(),
            $entity->getPhoto(),
            $entity->getRol(),
            $entity->getStatus(),
            $entity->getType(),
            $entity->getUsername(),
            $entity->getWebrol(),
            $entity->getSalt()
        );

        $current_pass = $entity->getPassword();

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            // Changing password if needed
            if ($entity->getPassword() != "") {
                $entity->setCode($entity->getPassword());
                $this->setSecurePassword($entity);
            } else {
                $entity->setPassword($current_pass);
            }

            // Modifiying User Role
            if ($entity->getType() == 1) $entity->setRoles('ROLE_USER'); else $entity->setRoles('ROLE_ADMIN');

            // Timestamp
            $entity->setLastupdate(date('Y-m-d H:i:s'));

            $em->persist($user_archive);

            $em->flush();

            return $this->redirect($this->generateUrl('user_edit', array('id' => $id)));
        }

        return $this->render('PalmCRMBundle:User:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a User entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PalmCRMBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('user'));
    }

    /**
     * Creates a form to delete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

    private function setSecurePassword(&$entity)
    {
        $entity->setSalt(md5(time()));
        $encoder = new \Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder('sha512', true, 10);
        $password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
        $entity->setPassword($password);
    }

    /*
     * Update pasword for an user
     *
     */
    public function updatepassAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $current_password = $this->getRequest()->get('current_password');
        $new_password = $this->getRequest()->get('new_password');
        $new_password_confirm = $this->getRequest()->get('new_password_confirm');

        $entity = $em->getRepository('PalmCRMBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }

        $user_archive = new Userarchive(
            $entity->getCode(),
            $entity->getColor(),
            $entity->getEmail(),
            $entity->getFullname(),
            date('Y-m-d H:i:s'),
            $entity->getPassword(),
            $entity->getPhone(),
            $entity->getPhoto(),
            $entity->getRol(),
            $entity->getStatus(),
            $entity->getType(),
            $entity->getUsername(),
            $entity->getWebrol(),
            $entity->getSalt()
        );

        // Check if current password is OK
        $encoder_service = $this->get('security.encoder_factory');
        $encoder = $encoder_service->getEncoder($entity);
        $encoded_pass = $encoder->encodePassword($current_password, $entity->getSalt());

        if ($encoded_pass != $entity->getPassword())
            $error[] = 'Current password incorrect';

        if ($new_password != $new_password_confirm)
            $error[] = 'Passwords are not the same';

        if (!isset($error)) {
            // Changing password
            $entity->setCode($new_password);
            $entity->setPassword($new_password);
            $this->setSecurePassword($entity);
            $em->persist($entity);
            $em->persist($user_archive);
            $em->flush();

            $error[] = 'Password change success!';
            return $this->render('PalmCRMBundle:User:show.html.twig', array(
                'id' => $id,
                'error' => $error,
                'entity' => $entity
            ));
        } else {
            return $this->render('PalmCRMBundle:User:show.html.twig', array(
                'error' => $error,
                'entity' => $entity
            ));
        }
    }

    /**
     * Edits an existing User entity Ajax.
     *
     */
    public function updateAjaxAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PalmCRMBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $user_archive = new Userarchive(
            $entity->getCode(),
            $entity->getColor(),
            $entity->getEmail(),
            $entity->getFullname(),
            date('Y-m-d H:i:s'),
            $entity->getPassword(),
            $entity->getPhone(),
            $entity->getPhoto(),
            $entity->getRol(),
            $entity->getStatus(),
            $entity->getType(),
            $entity->getUsername(),
            $entity->getWebrol(),
            $entity->getSalt()
        );

        $field = $this->getRequest()->get('name');
        $value = $this->getRequest()->get('value');

        if (!$field || !$value)
            exit;

        switch ($field) {
            case 'full_name':
                $entity->setFullname($value);
                break;
            case 'email':
                $entity->setEmail($value);
                break;
        }

        $em->persist($user_archive);
        $em->flush();

        exit;
    }
}
