<?php

namespace Palm\CRMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Palm\CRMBundle\Entity\View;
use Palm\CRMBundle\Form\ViewType;

/**
 * View controller.
 *
 */
class ViewController extends Controller
{

    /**
     * Lists all View entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PalmCRMBundle:View')->findAll();

        return $this->render('PalmCRMBundle:View:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new View entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new View();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('view_show', array('id' => $entity->getId())));
        }

        return $this->render('PalmCRMBundle:View:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a View entity.
    *
    * @param View $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(View $entity)
    {
        $form = $this->createForm(new ViewType(), $entity, array(
            'action' => $this->generateUrl('view_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new View entity.
     *
     */
    public function newAction()
    {
        $entity = new View();
        $form   = $this->createCreateForm($entity);

        return $this->render('PalmCRMBundle:View:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a View entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PalmCRMBundle:View')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find View entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PalmCRMBundle:View:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing View entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PalmCRMBundle:View')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find View entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PalmCRMBundle:View:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a View entity.
    *
    * @param View $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(View $entity)
    {
        $form = $this->createForm(new ViewType(), $entity, array(
            'action' => $this->generateUrl('view_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing View entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PalmCRMBundle:View')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find View entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('view_edit', array('id' => $id)));
        }

        return $this->render('PalmCRMBundle:View:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a View entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PalmCRMBundle:View')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find View entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('view'));
    }

    /**
     * Creates a form to delete a View entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('view_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
