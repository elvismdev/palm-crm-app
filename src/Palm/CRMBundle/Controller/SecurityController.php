<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Palm\CRMBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;


class SecurityController extends Controller
{
    public function loginAction()
    {
        $peticion = $this->getRequest();

        $sesion = $peticion->getSession();

        if ($peticion->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $peticion->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $sesion->get(SecurityContext::AUTHENTICATION_ERROR);
        }
        return $this->render('PalmCRMBundle:Security:login.html.twig', array(
            'last_username' => $sesion->get(SecurityContext::LAST_USERNAME),
            'error' => $error,
        ));
    }

    public function nonaccessAction()
    {
        // Log the user out
        $this->get("request")->getSession()->invalidate();
        $this->get("security.context")->setToken(null);

        return $this->redirect($this->generateUrl('Login', array('nonaccess' => true)));
    }

    public function passforgotAction()
    {
        $em = $this->getDoctrine()->getManager();

        $email = $this->get('request')->request->get('email');

        if (!$email)
            return $this->redirect($this->generateUrl('Login'));

        $entity = $em->getRepository('PalmCRMBundle:User')->findOneBy(array('email' => $email));

        if (!$entity) {
            return $this->redirect($this->generateUrl('Login'));
        }

        $mailer = $em->getRepository('PalmCRMBundle:Mailer')->findOneBySource('PasswordForgot');

        $amazonaws = $em->getRepository('PalmCRMBundle:Amazonaws')->findOneById(1);

        require_once($this->get('kernel')->getRootDir() . '/../vendor/amazonsesmailer/AmazonSESMailer.php');

        $amazonSESMailer = new \AmazonSESMailer($amazonaws->getAwsId(), $amazonaws->getAwsSecretKey());

        $amazonSESMailer->ClearReplyTos();
        $amazonSESMailer->AddReplyTo($mailer->getFrom());

        $amazonSESMailer->SetFrom($mailer->getFrom(), $mailer->getFromname());

        // ADD THE TO EMAILS
        $amazonSESMailer->AddAddress($email);

        // SUBJECT
        $amazonSESMailer->Subject = $mailer->getSubject();

        // MESSAGE
        $body = $mailer->getBody();

        $body = str_replace('[fullname]', $entity->getFullname(), $body);
        $body = str_replace('[code]', $entity->getCode(), $body);

        $amazonSESMailer->MsgHtml($body);

        $amazonSESMailer->Send();

        $this->get('twig')->addGlobal('emailsent', $email);

        return $this->render('PalmCRMBundle:Security:login.html.twig', array(
            'last_username' => '',
            'error' => '',
            'emailsent' => $email
        ));
    }
}
?>