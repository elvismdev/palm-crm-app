<?php

namespace Palm\CRMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('PalmCRMBundle:Default:index.html.twig');
    }

    public function dashboardAction()
    {
        return $this->indexAction();
    }

    public function escape($string)
    {
        return '"' . $string . '"';
    }

    public function filter($string)
    {
        if (!is_array($string))
            return (strlen($string) > 0) ? true : false;

        return ($string || $string == 0) ? true : false;
    }

    public function replace($string)
    {
        $to_replace = array(
            '\\/' => '/',
            '\"' => '"'
        );

        foreach ($to_replace as $serach => $replace) {
            $string = str_replace($serach, $replace, $string);
        }

        return $string;
    }

    /*
     * JSON json_encode()
     */
    public function jsonAction()
    {
        $providers = array();

        $em = $this->getDoctrine()->getManager();

        $containers_db = $em->getRepository('PalmCRMBundle:Container')->findAll();

        foreach ($containers_db as $c) {
            $views_db = $em->getRepository('PalmCRMBundle:View')->findByContainer($c);

            foreach ($views_db as $v) {
                $elements_db = $em->getRepository('PalmCRMBundle:Element')->findByView($v);

                foreach ($elements_db as $e) {
                    if (!$e->getStatus())
                        continue;

                    if (in_array($e->getType(), array('CB', 'DD'))) {
                        $elements[] = array_filter(array(
                            'type' => $e->getType(),
                            'left' => $e->getLeft(),
                            'top' => $e->getTop(),
                            'right' => $e->getRight(),
                            'field_id' => (in_array($e->getType(), array('CB', 'TF', 'TA', 'DD'))) ? "" . $e->getId() . "" : '',
                            'value' => $e->getValue(),
                            'lbl' => array_filter(array(
                                'left' => $e->getLblleft(),
                                'top' => $e->getLbltop(),
                                'font' => array_filter(array(
                                    'fontFamily' => $e->getFontfamily(),
                                    'fontSize' => $e->getFontsize(),
                                    'fontWeight' => $e->getFontweight())),
                                'color' => $e->getColor(),
                                'text' => $e->getText(),
                                'height' => $e->getHeight(),
                                'width' => $e->getWidth(),
                            )),
                            'backgroundImage' => $e->getBackgroundimage(),
                            'backgroundFocusedImage' => $e->getBackgroundfocusedimage(),
                            'backgroundColor' => $e->getBackgroundcolor(),
                            'ID' => $e->getButtonid(),
                            'options' => $e->getOptions(),
                            'hintText' => $e->getHinttext(),
                            'borderColor' => $e->getBordercolor(),
                            'borderRadius' => $e->getBorderradius(),
                            'paddingLeft' => $e->getPaddingleft(),
                        ), array($this, 'filter'));
                    } else {
                        $elements[] = array_filter(array(
                            'type' => $e->getType(),
                            'left' => $e->getLeft(),
                            'top' => $e->getTop(),
                            'right' => $e->getRight(),
                            'field_id' => (in_array($e->getType(), array('CB', 'TF', 'TA', 'DD'))) ? "" . $e->getId() . "" : '',
                            'value' => $e->getValue(),
                            'font' => array_filter(array(
                                'fontFamily' => $e->getFontfamily(),
                                'fontSize' => $e->getFontsize(),
                                'fontWeight' => $e->getFontweight())),
                            'color' => $e->getColor(),
                            'text' => $e->getText(),
                            'height' => $e->getHeight(),
                            'width' => $e->getWidth(),
                            'backgroundImage' => $e->getBackgroundimage(),
                            'backgroundFocusedImage' => $e->getBackgroundfocusedimage(),
                            'backgroundColor' => $e->getBackgroundcolor(),
                            'ID' => $e->getButtonid(),
                            'options' => $e->getOptions(),
                            'hintText' => $e->getHinttext(),
                            'borderColor' => $e->getBordercolor(),
                            'borderRadius' => $e->getBorderradius(),
                            'paddingLeft' => $e->getPaddingleft(),
                        ), array($this, 'filter'));
                    }
                }

                if ($v->getStatus()) {
                    $providers['providers'][]['Containers']['views'][] = array_filter(array(
                        'screen' => "" . $v->getScreen() . "",
                        'layout' => $v->getLayout(),
                        'left' => $v->getLeft(),
                        'width' => $v->getWidth(),
                        'height' => $v->getHeight(),
                        'top' => $v->getTop(),
                        'visible' => $v->getVisible(),
                        'Elements' => $elements), array($this, 'filter'));
                }

                unset($elements);
            }
        }

        $response = new Response($this->replace(json_encode($providers, JSON_PRETTY_PRINT)));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
