<?php

namespace Palm\CRMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Filesystem\Filesystem;

use Palm\CRMBundle\Entity\Calendar;
use Palm\CRMBundle\Controller\EmailController;

class CalendarController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQuery('SELECT u FROM PalmCRMBundle:User u
              JOIN PalmCRMBundle:Calendar c
              WHERE c.userid = u.id
              AND u.color != :nothing')
            ->setParameter('nothing', '')
            ->getResult();

        $user_legend = $event_list = '';
        foreach ($entities as $e) {
            $user_legend[] = array('color' => $e->getColor(), 'fullname' => $e->getFullname());

            $events = $em->createQuery('SELECT c FROM PalmCRMBundle:Calendar c
                WHERE c.userid = :userid AND c.status = 1')
                ->setParameter('userid', $e->getId())
                ->getResult();

            foreach ($events as $ev) {
                $ev->doctor = $em->getRepository('PalmCRMBundle:Doctors')->find($ev->getDoctorid());

                $last_name = $ev->doctor->getLastname();
                $first_name = $ev->doctor->getFirstname();
                $middle_name = $ev->doctor->getMiddlename();
                $app_datetime = $ev->getAppdatetime();
                $color = $e->getColor();
                $event_id = $ev->getId();

                // { title: 'Test', start: '2014-06-09T16:00:00', end: '2014-06-09T16:00:00', allDay: false, className: 'label-important' }
                $event_list[] = array(
                    'title' => "$last_name, $first_name $middle_name",
                    'start' => $app_datetime,
                    'end' => $app_datetime,
                    'allDay' => 'false',
                    'className' => "label-$color",
                    'color' => $event_id
                );
            }

        }

        $status = '';
        if (isset($_SESSION['status'])) {
            $status = $_SESSION['status'];
            unset($_SESSION['status']);
        }

        $intervals = $this->hoursRange( 28800, 75600, 60 * 15 );

        $providers = $em->createQuery('SELECT d FROM PalmCRMBundle:Doctors d ORDER BY d.lastname ASC, d.firstname ASC, d.middlename ASC')
            ->getResult();

        return $this->render('PalmCRMBundle:Calendar:index.html.twig', array(
            'entities' => $entities,
            'user_legend' => $user_legend,
            'event_list' => $event_list,
            'intervals' => $intervals,
            'providers' => $providers,
            'status' => $status,
        ));
    }

    /**
     * Disable a Calendar entity.
     *
     */
    public function disableAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('PalmCRMBundle:Calendar')->find($id);

        if (!$entity) {
            exit;
        }

        $entity->setStatus(0);
        $em->flush();

        $user = $em->getRepository('PalmCRMBundle:User')->find($entity->getUserid());
        $provider = $em->getRepository('PalmCRMBundle:Doctors')->find($entity->getDoctorid());

        $email = new EmailController($em, $this->get('kernel')->getRootDir());
        $email->EmailNotificationAppointment($user, $provider, $entity, false);

        exit;
    }

    public function createApiAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PalmCRMBundle:Calendar')->find($id);

        if (!$entity) {
            $response = new Response('Unable to find Calendar entity!!!!');
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }

        $user = $em->getRepository('PalmCRMBundle:User')->find($entity->getUserid());
        $provider = $em->getRepository('PalmCRMBundle:Doctors')->find($entity->getDoctorid());

        $email = new EmailController($em, $this->get('kernel')->getRootDir());
        $email->EmailNotificationAppointment($user, $provider, $entity, true);

        $response = new Response('Email sent Ok!!!!');
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function createAction()
    {
        $date = $this->get('request')->request->get('date');
        $time = $this->get('request')->request->get('time');
        $id = $this->get('request')->request->get('id');

        if (!$date || !$time)
            return $this->redirect($this->generateUrl('doctors_edit', array('id' => $id)));

        $user = $this->get('security.context')->getToken()->getUser();

        $entity = new Calendar($date . ' ' . $time, $id, 1, $user->getId(), date('Y-m-d H:i:s'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        $_SESSION['status'] = 'newAppointment';

        $mailer = $em->getRepository('PalmCRMBundle:Mailer')->findOneBySource('AppointmentDetails');
        $provider = $em->getRepository('PalmCRMBundle:Doctors')->findOneById($id);

        $email = new EmailController($em, $this->get('kernel')->getRootDir());
        $email->EmailNotificationAppointment($user, $provider, $entity, true);

        if (strstr($this->getRequest()->headers->get('referer'), 'Calendar'))
            return $this->redirect($this->generateUrl('calendar'));
        return $this->redirect($this->generateUrl('doctors_edit', array('id' => $id)));
    }

    public function hoursRange( $lower = 28800, $upper = 86400, $step = 3600, $format = '' ) {
        $times = array();

        if ( empty( $format ) ) {
            $format = 'g:i a';
        }

        foreach ( range( $lower, $upper, $step ) as $increment ) {
            $increment = gmdate( 'H:i', $increment );

            list( $hour, $minutes ) = explode( ':', $increment );

            $date = new \DateTime( $hour . ':' . $minutes );

            $times[(string) $increment] = $date->format( $format );
        }

        return $times;
    }

}
