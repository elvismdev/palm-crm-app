<?php

namespace Palm\CRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Element
 */
class Element
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $left;

    /**
     * @var string
     */
    private $top;

    /**
     * @var string
     */
    private $lblleft;

    /**
     * @var string
     */
    private $lbltop;

    /**
     * @var string
     */
    private $color;

    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $fontfamily;

    /**
     * @var string
     */
    private $fontsize;

    /**
     * @var \Palm\CRMBundle\Entity\View
     */
    private $view;

    /**
     * @var string
     */
    private $fontweight;

    /**
     * @var string
     */
    private $value;

    /**
     * @var string
     */
    private $height;

    /**
     * @var string
     */
    private $width;

    /**
     * @var string
     */
    private $right;

    /**
     * @var string
     */
    private $backgroundimage;

    /**
     * @var string
     */
    private $backgroundfocusedimage;

    /**
     * @var string
     */
    private $backgroundcolor;

    /**
     * @var integer
     */
    private $buttonid;

    /**
     * @var string
     */
    private $options;

    /**
     * @var string
     */
    private $hinttext;

    /**
     * @var string
     */
    private $bordercolor;

    /**
     * @var string
     */
    private $borderradius;

    /**
     * @var string
     */
    private $paddingleft;

    /**
     * @var integer
     */
    private $status;

    /**
     * Set id
     *
     * @param integer $id
     * @return Element
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Element
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set left
     *
     * @param string $left
     * @return Element
     */
    public function setLeft($left)
    {
        $this->left = $left;

        return $this;
    }

    /**
     * Get left
     *
     * @return string
     */
    public function getLeft()
    {
        return $this->left;
    }

    /**
     * Set top
     *
     * @param string $top
     * @return Element
     */
    public function setTop($top)
    {
        $this->top = $top;

        return $this;
    }

    /**
     * Get top
     *
     * @return string
     */
    public function getTop()
    {
        return $this->top;
    }

    /**
     * Set lblleft
     *
     * @param string $lblleft
     * @return Element
     */
    public function setLblleft($lblleft)
    {
        $this->lblleft = $lblleft;

        return $this;
    }

    /**
     * Get lblleft
     *
     * @return string
     */
    public function getLblleft()
    {
        return $this->lblleft;
    }

    /**
     * Set lbltop
     *
     * @param string $lbltop
     * @return Element
     */
    public function setLbltop($lbltop)
    {
        $this->lbltop = $lbltop;

        return $this;
    }

    /**
     * Get lbltop
     *
     * @return string
     */
    public function getLbltop()
    {
        return $this->lbltop;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return Element
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Element
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set fontfamily
     *
     * @param string $fontfamily
     * @return Element
     */
    public function setFontfamily($fontfamily)
    {
        $this->fontfamily = $fontfamily;

        return $this;
    }

    /**
     * Get fontfamily
     *
     * @return string
     */
    public function getFontfamily()
    {
        return $this->fontfamily;
    }

    /**
     * Set fontsize
     *
     * @param string $fontsize
     * @return Element
     */
    public function setFontsize($fontsize)
    {
        $this->fontsize = $fontsize;

        return $this;
    }

    /**
     * Get fontsize
     *
     * @return string
     */
    public function getFontsize()
    {
        return $this->fontsize;
    }

    /**
     * Set view
     *
     * @param \Palm\CRMBundle\Entity\View $view
     * @return Element
     */
    public function setView(\Palm\CRMBundle\Entity\View $view = null)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Get view
     *
     * @return \Palm\CRMBundle\Entity\View
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set fontweight
     *
     * @param string $fontweight
     * @return Element
     */
    public function setFontweight($fontweight)
    {
        $this->fontweight = $fontweight;

        return $this;
    }

    /**
     * Get fontweight
     *
     * @return string
     */
    public function getFontweight()
    {
        return $this->fontweight;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Element
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set height
     *
     * @param string $height
     * @return Element
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set right
     *
     * @param string $right
     * @return Element
     */
    public function setRight($right)
    {
        $this->right = $right;

        return $this;
    }

    /**
     * Get right
     *
     * @return string
     */
    public function getRight()
    {
        return $this->right;
    }

    /**
     * Set width
     *
     * @param string $width
     * @return Element
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return string
     */
    public function getWidth()
    {
        return $this->width;
    }

    public function setBackgroundimage($backgroundimage)
    {
        $this->backgroundimage = $backgroundimage;

        return $this;
    }

    public function getBackgroundimage()
    {
        return $this->backgroundimage;
    }

    public function setBackgroundfocusedimage($backgroundfocusedimage)
    {
        $this->backgroundfocusedimage = $backgroundfocusedimage;

        return $this;
    }

    public function getBackgroundfocusedimage()
    {
        return $this->backgroundfocusedimage;
    }

    public function setBackgroundcolor($backgroundcolor)
    {
        $this->backgroundcolor = $backgroundcolor;

        return $this;
    }

    public function getBackgroundcolor()
    {
        return $this->backgroundcolor;
    }

    public function setButtonid($buttonid)
    {
        $this->buttonid = $buttonid;

        return $this;
    }

    public function getButtonid()
    {
        return $this->buttonid;
    }

    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function setHinttext($hinttext)
    {
        $this->hinttext = $hinttext;

        return $this;
    }

    public function getHinttext()
    {
        return $this->hinttext;
    }

    public function setBordercolor($bordercolor)
    {
        $this->bordercolor = $bordercolor;

        return $this;
    }

    public function getBordercolor()
    {
        return $this->bordercolor;
    }

    public function setBorderradius($borderradius)
    {
        $this->borderradius = $borderradius;

        return $this;
    }

    public function getBorderradius()
    {
        return $this->borderradius;
    }

    public function setPaddingleft($paddingleft)
    {
        $this->paddingleft = $paddingleft;

        return $this;
    }

    public function getPaddingleft()
    {
        return $this->paddingleft;
    }

     public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

}
