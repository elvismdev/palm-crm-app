<?php
/**
 * Created by PhpStorm.
 * User: l3yr0y
 * Date: 25/06/14
 * Time: 21:01
 */

namespace Palm\CRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Specialty
 */
class Specialty
{
    private $id;
    private $specialtyvalue;
    private $specialtydesc;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $specialtydesc
     */
    public function setSpecialtydesc($specialtydesc)
    {
        $this->specialtydesc = $specialtydesc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpecialtydesc()
    {
        return $this->specialtydesc;
    }

    /**
     * @param mixed $specialtyvalue
     */
    public function setSpecialtyvalue($specialtyvalue)
    {
        $this->specialtyvalue = $specialtyvalue;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpecialtyvalue()
    {
        return $this->specialtyvalue;
    }


} 