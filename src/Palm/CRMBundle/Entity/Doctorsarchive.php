<?php

namespace Palm\CRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Doctorsarchive
 */
class Doctorsarchive
{
    private $id;
    private $location;
    private $lastname;
    private $firstname;
    private $middlename;
    private $degree;
    private $specialty;
    private $officeaddress1;
    private $officeaddress2;
    private $officecity;
    private $officestate;
    private $officezip;
    private $officetel;
    private $officefax;
    private $officemanager;
    private $comments;
    private $emailaddress;
    private $lastupdate;
    private $edittime;
    private $whoupdate;

    function __construct($comments, $degree, $edittime, $emailaddress, $firstname, $lastname, $lastupdate, $location, $middlename, $officeaddress1, $officeaddress2, $officecity, $officefax, $officemanager, $officestate, $officetel, $officezip, $specialty, $whoupdate)
    {
        $this->comments = $comments;
        $this->degree = $degree;
        $this->edittime = $edittime;
        $this->emailaddress = $emailaddress;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->lastupdate = $lastupdate;
        $this->location = $location;
        $this->middlename = $middlename;
        $this->officeaddress1 = $officeaddress1;
        $this->officeaddress2 = $officeaddress2;
        $this->officecity = $officecity;
        $this->officefax = $officefax;
        $this->officemanager = $officemanager;
        $this->officestate = $officestate;
        $this->officetel = $officetel;
        $this->officezip = $officezip;
        $this->specialty = $specialty;
        $this->whoupdate = $whoupdate;
    }


    /**
     * @param mixed $edittime
     */
    public function setEdittime($edittime)
    {
        $this->edittime = $edittime;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEdittime()
    {
        return $this->edittime;
    }

    /**
     * @param mixed $whoupdate
     */
    public function setWhoupdate($whoupdate)
    {
        $this->whoupdate = $whoupdate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWhoupdate()
    {
        return $this->whoupdate;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function setMiddlename($middlename)
    {
        $this->middlename = $middlename;

        return $this;
    }

    public function setDegree($degree)
    {
        $this->degree = $degree;

        return $this;
    }

    public function setSpecialty($specialty)
    {
        $this->specialty = $specialty;

        return $this;
    }

    public function setOfficeaddress1($officeaddress1)
    {
        $this->officeaddress1 = $officeaddress1;

        return $this;
    }

    public function setOfficeaddress2($officeaddress2)
    {
        $this->officeaddress2 = $officeaddress2;

        return $this;
    }

    public function setOfficecity($officecity)
    {
        $this->officecity = $officecity;

        return $this;
    }

    public function setOfficestate($officestate)
    {
        $this->officestate = $officestate;

        return $this;
    }

    public function setOfficezip($officezip)
    {
        $this->officezip = $officezip;

        return $this;
    }

    public function setOfficetel($officetel)
    {
        $this->officetel = $officetel;

        return $this;
    }

    public function setOfficefax($officefax)
    {
        $this->officefax = $officefax;

        return $this;
    }

    public function setOfficemanager($officemanager)
    {
        $this->officemanager = $officemanager;

        return $this;
    }

    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    public function setEmailaddress($emailaddress)
    {
        $this->emailaddress = $emailaddress;

        return $this;
    }

    public function setLastupdate($lastupdate)
    {
        $this->lastupdate = $lastupdate;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return mixed
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * @return mixed
     */
    public function getEmailaddress()
    {
        return $this->emailaddress;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return mixed
     */
    public function getLastupdate()
    {
        return $this->lastupdate;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @return mixed
     */
    public function getMiddlename()
    {
        return $this->middlename;
    }

    /**
     * @return mixed
     */
    public function getOfficeaddress1()
    {
        return $this->officeaddress1;
    }

    /**
     * @return mixed
     */
    public function getOfficeaddress2()
    {
        return $this->officeaddress2;
    }

    /**
     * @return mixed
     */
    public function getOfficecity()
    {
        return $this->officecity;
    }

    /**
     * @return mixed
     */
    public function getOfficefax()
    {
        return $this->officefax;
    }

    /**
     * @return mixed
     */
    public function getOfficemanager()
    {
        return $this->officemanager;
    }

    /**
     * @return mixed
     */
    public function getOfficestate()
    {
        return $this->officestate;
    }

    /**
     * @return mixed
     */
    public function getOfficetel()
    {
        return $this->officetel;
    }

    /**
     * @return mixed
     */
    public function getOfficezip()
    {
        return $this->officezip;
    }

    /**
     * @return mixed
     */
    public function getSpecialty()
    {
        return $this->specialty;
    }

}