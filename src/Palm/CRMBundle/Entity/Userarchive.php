<?php

namespace Palm\CRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Usuarioarchive
 */
class Userarchive  {

    /**
     * @var integer
     */
    protected $id;

    protected $username;
    protected $fullname;
    protected $password;
    protected $photo;
    protected $phone;
    protected $email;
    protected $code;
    protected $status;
    protected $type;
    protected $lastupdate;
    protected $rol;
    protected $webrol;
    protected $color;
    protected $salt;

    function __construct($code, $color, $email, $fullname, $lastupdate, $password, $phone, $photo, $rol, $status, $type, $username, $webrol, $salt)
    {
        $this->code = $code;
        $this->color = $color;
        $this->email = $email;
        $this->fullname = $fullname;
        $this->lastupdate = $lastupdate;
        $this->password = $password;
        $this->phone = $phone;
        $this->photo = $photo;
        $this->rol = $rol;
        $this->status = $status;
        $this->type = $type;
        $this->username = $username;
        $this->webrol = $webrol;
        $this->salt = $salt;
    }

    public function getUsername() {
        return $this->email;
    }

    public function getNickname() {
        return $this->username;
    }

    /**
     * @inheritDoc
     */
    public function getSalt() {
        return $this->salt;
    }

    /**
     * @inheritDoc
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials() {

    }

    public function getFullname() {
        return $this->fullname;
    }

    public function getPhoto() {
        return $this->photo;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getCode() {
        return $this->code;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getType() {
        return $this->type;
    }

    public function getLastupdate() {
        return $this->lastupdate;
    }

    public function getRoles() {
        return array($this->rol);
    }

    public function getRol() {
        return $this->rol;
    }

    public function getWebrol() {
        return $this->webrol;
    }

    public function getColor() {
        return $this->color;
    }

    public function setUsername($user) {
        $this->username = $user;
    }

    public function setPassword($pass) {
        $this->password = $pass;
    }

    public function setSalt($salt) {
        $this->salt = $salt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    public function setId(\Palm\CRMBundle\Entity\Userarchive $id = null) {
        $this->id = $id;

        return $this;
    }

    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function setLastupdate($lastupdate)
    {
        $this->lastupdate = $lastupdate;

        return $this;
    }

    public function setRoles($rol)
    {
        $this->rol = $rol;

        return $this;
    }

    public function setWebrol($webrol)
    {
        $this->webrol = $webrol;

        return $this;
    }

    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    public function __toString() {
        return $this->username;
    }
}
