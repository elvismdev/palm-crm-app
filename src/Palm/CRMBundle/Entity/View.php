<?php

namespace Palm\CRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * View
 */
class View
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $screen;

    /**
     * @var string
     */
    private $layout;

    /**
     * @var string
     */
    private $left;

    /**
     * @var string
     */
    private $width;

    /**
     * @var string
     */
    private $height;

    /**
     * @var string
     */
    private $top;

    /**
     * @var string
     */
    private $visible;

    /**
     * @var tinyint
     */
    private $status;

    /**
     * @var \Palm\CRMBundle\Entity\Container
     */
    private $container;


    /**
     * Set id
     *
     * @param integer $id
     * @return View
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set screen
     *
     * @param integer $screen
     * @return View
     */
    public function setScreen($screen)
    {
        $this->screen = $screen;

        return $this;
    }

    /**
     * Get screen
     *
     * @return integer
     */
    public function getScreen()
    {
        return $this->screen;
    }

    /**
     * Set layout
     *
     * @param string $layout
     * @return View
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;

        return $this;
    }

    /**
     * Get layout
     *
     * @return string
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * Set left
     *
     * @param string $left
     * @return View
     */
    public function setLeft($left)
    {
        $this->left = $left;

        return $this;
    }

    /**
     * Get left
     *
     * @return string
     */
    public function getLeft()
    {
        return $this->left;
    }

    /**
     * Set width
     *
     * @param string $width
     * @return View
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return string
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param string $height
     * @return View
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set top
     *
     * @param string $top
     * @return View
     */
    public function setTop($top)
    {
        $this->top = $top;

        return $this;
    }

    /**
     * Get top
     *
     * @return string
     */
    public function getTop()
    {
        return $this->top;
    }

    /**
     * Set visible
     *
     * @param string $visible
     * @return View
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return View
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get visible
     *
     * @return string
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set container
     *
     * @param \Palm\CRMBundle\Entity\Container $container
     * @return View
     */
    public function setContainer(\Palm\CRMBundle\Entity\Container $container = null)
    {
        $this->container = $container;

        return $this;
    }

    /**
     * Get container
     *
     * @return \Palm\CRMBundle\Entity\Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    public function __toString()
    {
        return (string) $this->screen;
    }
}
