<?php

namespace Palm\CRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Calendar
 */
class Calendar {
    private $id;
    private $userid;
    private $doctorid;
    private $appdatetime;
    private $status;
    private $lastupdate;

    function __construct($appdatetime, $doctorid, $status, $userid, $lastupdate)
    {
        $this->appdatetime = $appdatetime;
        $this->doctorid = $doctorid;
        $this->status = $status;
        $this->userid = $userid;
        $this->lastupdate = $lastupdate;
    }

    /**
     * @param mixed $appdatetime
     */
    public function setAppdatetime($appdatetime)
    {
        $this->appdatetime = $appdatetime;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAppdatetime()
    {
        return $this->appdatetime;
    }

    /**
     * @param mixed $doctorid
     */
    public function setDoctorid($doctorid)
    {
        $this->doctorid = $doctorid;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDoctorid()
    {
        return $this->doctorid;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $userid
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param mixed $lastupdate
     */
    public function setLastupdate($lastupdate)
    {
        $this->lastupdate = $lastupdate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastupdate()
    {
        return $this->lastupdate;
    }

}