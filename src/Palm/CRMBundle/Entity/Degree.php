<?php

namespace Palm\CRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Degree
 */
class Degree
{
    private $id;
    private $degreevalue;
    private $degreedesc;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $degreedesc
     */
    public function setDegreedesc($degreedesc)
    {
        $this->degreedesc = $degreedesc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDegreedesc()
    {
        return $this->degreedesc;
    }

    /**
     * @param mixed $degreevalue
     */
    public function setDegreevalue($degreevalue)
    {
        $this->degreevalue = $degreevalue;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDegreevalue()
    {
        return $this->degreevalue;
    }


} 